{-# LANGUAGE RecordWildCards #-}
module Main where

import            Control.Monad               (unless, forM, forever)
import            Control.Monad.IO.Class      (liftIO)
import            Control.Monad.Except        (runExceptT)
import            Control.Monad.State         (StateT(..), lift, state, gets, modify, evalStateT, get, put)
import            Data.Either                 (partitionEithers)
import            Data.Ix                     (inRange)
import            Data.Map.Strict             (assocs)
import            Text.Read                   (readMaybe)

import            DualReader.Book
import            DualReader.Book.Sync
import            DualReader.Book.State
import            DualReader.Exit
import            DualReader.GetArguments
import            DualReader.PrettyPrint
import            DualReader.Reader
import            DualReader.UI

data TocItem = 
    TocItem 
    { chapterNum            :: Int
    , chapterName           :: String
    , chapterNesting        :: Int
    , chapterFirstParagraph :: Int
    }

data AppState = 
    AppState
    { bookContext           :: BookContext
    , isTableOfContentShown :: Bool
    , asTableOfContent      :: [TocItem]
    , activeItem            :: Int
    , numberOfItems         :: Int
    , firstVisibleItem      :: Int
    }

changeTocItem :: (Int -> Int) -> App ()
changeTocItem f = do
    s@AppState{..} <- get
    (_, height) <- lift windowSize
    let n = f activeItem
    let activeItem' = 
            case () of
              () | n < 0              -> 0
                 | n >= numberOfItems -> activeItem
                 | otherwise          -> n
    let firstVisibleItem' =
            case () of
              () | activeItem' < firstVisibleItem           -> activeItem'
                 | activeItem' >= firstVisibleItem + height -> activeItem' - height + 1
                 | otherwise                                -> firstVisibleItem
    put (s 
         { activeItem = activeItem' 
         , firstVisibleItem = firstVisibleItem' })

renderTableOfContent :: App [String]
renderTableOfContent = do
    AppState{..} <- get
    (width, height) <- lift windowSize
    pure $ 
        map (\TocItem{..} -> 
                take width 
                $ (if chapterNum == activeItem then ">> " else "   ") ++ chapterName) 
            (take height $ drop firstVisibleItem asTableOfContent)

type App = StateT AppState Display

getUserMarker :: Int -> Int -> IO UserSyncMark
getUserMarker num1 num2 = do
  putStrLn "Enter right association of two chapters"
  numStr <- getLine
  let maybe2num = sequence $ readMaybe <$> words numStr
  case maybe2num of
    Just [x, y] -> do
      unless (inRange (0, num1 - 1) x && inRange (0, num2 - 1) y) exitFailMarkerVal
      return (x, y)
    _                 -> exitFailMarker

getContextByMarker :: [Book] -> IO BookContext
getContextByMarker books = do
  let [tof1, tof2] = fmap (take 10 . assocs . tableOfContent) books
      (chapNum1, chapNum2) = (length tof1, length tof2)
      (book1 : book2 : _) = books
  if chapNum1 > 1 && chapNum2 > 1
    then do
      prettyPrint tof1 tof2
      userMarker <- getUserMarker chapNum1 chapNum2
      let markers = syncWithUserMarks book1 book2 userMarker
      return $ getBookContext book1 book2 markers
    else
      return $ getBookContext book1 book2 []

prepareTableOfContent :: BookContext -> [TocItem]
prepareTableOfContent context = toc'
  where
    toc = tableOfContent (book (mainBook context))
    toc' = map (\(i, (names, paragraph)) -> 
                    TocItem 
                    { chapterNum = i
                    , chapterName = if null names then "-/-" else last names
                    , chapterNesting = length names
                    , chapterFirstParagraph = paragraph
                    }) (assocs toc)

runApp :: BookContext -> App a -> IO a
runApp initialState app = runDisplay $ evalStateT app initialState'
  where
    toc = prepareTableOfContent initialState
    initialState' = AppState initialState False toc 0 (length toc) 0

cmdHandling :: Command -> App ()
cmdHandling Exit               = liftIO exitSucc

cmdHandling MoveToNextPage     = do 
        (w, h) <- lift windowSize 
        modify (\c -> c { bookContext = moveToNextLine w h (bookContext c) }) 

cmdHandling MoveToPreviousPage = do
        (w, h) <- lift windowSize 
        modify (\c -> c { bookContext = moveToPrevLine w h (bookContext c) }) 

cmdHandling SwapBooks          = modify (\c -> c { bookContext = swapBooks (bookContext c) })
cmdHandling ShowTableOfContent = modify (\c -> c { isTableOfContentShown = not (isTableOfContentShown c) })
cmdHandling None               = pure ()

cmdHandlingForToc :: Command -> App ()
cmdHandlingForToc Exit               = liftIO exitSucc
cmdHandlingForToc MoveToNextPage     = changeTocItem succ
cmdHandlingForToc MoveToPreviousPage = changeTocItem pred

cmdHandlingForToc SwapBooks          = do
    s@AppState{..} <- get
    let bc@BookContext{..} = bookContext
    let mb@BookState{..} = mainBook
    let TocItem{..} = asTableOfContent !! activeItem
    put $ 
        s 
        { bookContext = 
            bc 
            { mainBook = 
                mb
                { para = chapterFirstParagraph
                , strNum = 0
                }
            , isMainFront = True 
            }
        , isTableOfContentShown = False 
        }

cmdHandlingForToc ShowTableOfContent = modify (\c -> c { isTableOfContentShown = not (isTableOfContentShown c) })
cmdHandlingForToc None               = pure ()

main :: IO ()
main = do
  files <- getPaths
  eitherBooks <- forM files $ runExceptT . readBook
  let (errs, books) = partitionEithers eitherBooks
  unless (null errs) $ exitFailReadBook (unlines errs)
  initialState <- getContextByMarker books
  runApp initialState $ forever $ do
    b <- gets isTableOfContentShown
    if b
        then do
            _ <- lift windowSize
            page <- renderTableOfContent
            lift $ displayPage "Table of content" "" page
            cmd <- lift readCommand
            cmdHandlingForToc cmd
        else do
            (width, height) <- lift windowSize
            page <- state (\c -> 
                        let (page, bookContext') = getContent (bookContext c) width height 
                        in (page, c { bookContext = bookContext' }) )
            name <- gets (getName . bookContext)
            status <- gets (getStatus . bookContext)
            lift $ displayPage name status page
            cmd <- lift readCommand
            cmdHandling cmd
