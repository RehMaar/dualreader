module DualReader.GetArguments
  ( getPaths
  ) where

import           Control.Monad         (when, unless)
import           System.Console.GetOpt (OptDescr(..), getOpt, ArgDescr(..), ArgOrder(..), usageInfo)
import           System.Directory      (doesFileExist)
import           System.Environment    (getArgs)

import           DualReader.Exit


data Flag = Version
          | Help
  deriving (Show, Eq, Enum, Ord)


versionInfo :: String
versionInfo = "Haskell DualReader 0.1"

helpInfo :: String
helpInfo    = "Usage: dual-reader [-vh] [file1 file2]"

options :: [OptDescr Flag]
options =
  [ Option ['v'] ["version"] (NoArg Version) versionInfo
  , Option ['h'] ["help"]    (NoArg Help)    helpInfo
  ]

parseOpts :: [String] -> IO ([Flag], [String])
parseOpts argv =
  case getOpt Permute options argv of
    (flags, files, []) -> return (flags, files)
    (_, _, errs)       -> exitFailFlag $ concat errs ++ "\n" ++ usageInfo helpInfo options

checkExistence :: FilePath -> IO FilePath
checkExistence file = do
  isFileExist <- doesFileExist file
  unless isFileExist $ exitFailPath file
  return file

getPaths :: IO [FilePath]
getPaths = do
  (flags, files) <- getArgs >>= parseOpts
  when (Version `elem` flags) $ putStrLn versionInfo
  when (Help `elem` flags) $ putStrLn helpInfo
  when (length files /= 2) exitFailArgsNum
  mapM checkExistence files


