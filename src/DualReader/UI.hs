module DualReader.UI
    ( runDisplay
    , displayPage
    , readCommand
    , windowSize
    , Command (..)
    , Display
    ) where

import           Control.Monad                  (forM_)
import           Data.Function                  (fix)
import           UI.NCurses                     (Curses, Event (EventCharacter),
                                                 runCurses, moveCursor, drawString, setEcho,
                                                 defaultWindow, updateWindow, render,
                                                 getEvent, drawBox,
                                                 glyphLineV, glyphLineH, clear)
import qualified UI.NCurses                     as NC (windowSize)

data Command = Exit | MoveToNextPage | MoveToPreviousPage | SwapBooks | None | ShowTableOfContent

type Display = Curses

runDisplay :: Display a -> IO a
runDisplay display = runCurses $ do
    setEcho False
    display

displayPage :: String -> String -> [String] -> Display ()
displayPage name status rows = do
    w <- defaultWindow
    (width, height) <- windowSize
    updateWindow w $ do
        let lines' = take height rows
        clear
        drawBox v h 
        moveCursor 0 2
        drawString (take width name)
        height' <- fst <$> NC.windowSize
        moveCursor (height' - 1) 2
        drawString (take width status)
        forM_ (zip [1..height] lines') $ uncurry $ \n line -> do
            moveCursor (1 + fromIntegral n) 2
            drawString (take width line)
    render
  where
    v = Just glyphLineV
    h = Just glyphLineH

characterToCommand :: [(Char, Command)]
characterToCommand =
    [ ('q', Exit)
    , ('j', MoveToNextPage)
    , ('k', MoveToPreviousPage)
    , ('s', SwapBooks)
    , ('t', ShowTableOfContent)
    ]

readCommand :: Display Command
readCommand = do
    w <- defaultWindow
    fix $ \_ -> do
        ev <- getEvent w Nothing
        case ev of
          Just (EventCharacter c) ->
              case lookup c characterToCommand of
                Just command -> pure command
                Nothing      -> pure None
          _ -> pure None

windowSize :: Display (Int, Int)
windowSize = do
    w <- defaultWindow
    updateWindow w $ do
        (height, width) <- NC.windowSize
        pure (fromIntegral width - 4, fromIntegral height - 4)
