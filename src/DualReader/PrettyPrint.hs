module DualReader.PrettyPrint
  ( prettyPrint
  ) where

import           Data.List
import           Data.List.Split 
import           Prelude                hiding ((<>))
import           Text.PrettyPrint.Boxes


nameColWidth :: Int
nameColWidth = 42

numColWidth :: Int
numColWidth  = 4

type Content = [(Int, ([String], Int))]
type MainContent = [(String, String)]


prettyPrint :: Content -> Content -> IO ()
prettyPrint cont1 cont2 = printBox .table $ [".", getBookName cont1, ".", getBookName cont2] : zipContent (getPrintableContent cont1) (getPrintableContent cont2)

getBookName :: Content -> String
getBookName ((_, ((bookName : _), _)) : _) = bookName
getBookName _                              = "no name"

getPrintableContent :: Content -> MainContent
getPrintableContent = fmap (\(chapterNum, (sections, _)) -> go (show chapterNum ++ "\n") sections)
  where
    go :: String -> [String] -> (String, String)
    go num (_ : [])       = (num, "...")
    go num (_ : sections) = (num, last sections)
    go num _              = (num, "...")

zipContent :: [(String, String)] -> [(String, String)] -> [[String]]
zipContent []             []             = []
zipContent ((x, y) : xys) []             = [x, y, ".", "..."] : zipContent xys []
zipContent []             ((v, w) : vws) = [".", "...", v, w] : zipContent []  vws
zipContent ((x, y) : xys) ((v, w) : vws) = [x, y, v, w]   : zipContent xys vws


getCells :: Int -> [(Int, String)] -> [Box]
getCells width = fmap (vcat left . \(num, cell) -> columns left width num cell)

fmtColumn :: Int -> [(Int, String)] -> Box
fmtColumn width contCols = horizSep
                        // vcat left (intersperse horizSep cellList)
                        // horizSep
  where
    horizSep = text (replicate width '-')
    cellList = getCells width contCols

getNumRows :: [String] -> Int
getNumRows = maximum . fmap (length . lines)

table :: [[String]] -> Box
table contRows = verticSep
              <> hcat left (intersperse verticSep . fmap colsLogic $  dataCell)
              <> verticSep
  where
    formatting = fmap (unlines . chunksOf nameColWidth)
    contCols     = fmap formatting $ transpose contRows
    numRows      = fmap (getNumRows . formatting) contRows
    dataCell     = zip [1..] (fmap (zip numRows) contCols)
    verticSepStr = '+' : (intercalate "+" . fmap (flip replicate '|') $ numRows) ++ "+"
    verticSep    = vcat left . fmap char $ verticSepStr

colsLogic :: (Int, [(Int, String)]) -> Box
colsLogic (x, cont) = if odd x
                        then fmtColumn numColWidth cont
                        else fmtColumn nameColWidth cont

