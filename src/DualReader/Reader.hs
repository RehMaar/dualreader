module DualReader.Reader
  ( readBook
  ) where

import           Control.Applicative        ((<|>))
import           Control.Arrow              (first)
import           Control.Monad.Except       (ExceptT (..), liftIO, withExceptT)
import qualified Data.ByteString.Lazy       as BSL (ByteString, readFile)
import qualified Data.ByteString.Lazy.Char8 as BSLC8 (toStrict)
import           Data.List                  (nubBy)
import           Data.List.Split            (splitOn)
import qualified Data.Map.Strict            as M (fromList)
import           Data.Text                  (unpack)
import qualified Data.Text.Encoding         as DTE (decodeUtf8)
import           DualReader.Book            (Book (..), ParaNumber, Paragraph)
import           Text.Pandoc                (Block (..), Inline, 
                                             Pandoc (..), PandocError, 
                                             PandocPure, def, docTitle,
                                             readEPUB, readFB2, runIO, runPure,
                                             writePlain)

type HeaderName = (Int, String)

readBook :: FilePath -> ExceptT String IO Book
readBook pathToBook = do
    bookAsBS   <- liftIO $ BSL.readFile pathToBook
    pandocBook <- readEPUB' bookAsBS <|> readFB2' bookAsBS

    toExceptT . return . runPure $ getChapters pandocBook
  where
    readEPUB' :: BSL.ByteString -> ExceptT String IO Pandoc
    readEPUB' = toExceptT . runIO . readEPUB def

    readFB2' :: BSL.ByteString -> ExceptT String IO Pandoc
    readFB2' = toExceptT . runIO . readFB2 def . DTE.decodeUtf8 . BSLC8.toStrict

    toExceptT :: Monad m => m (Either PandocError a) -> ExceptT String m a
    toExceptT = withExceptT show . ExceptT

getChapters :: Pandoc -> PandocPure Book
getChapters (Pandoc meta blocks) = do
    paragraphsWithChapters           <- fmap (first (fmap snd)) <$> recursiveGetChapters [] blocks []
    let (linearParagraphs, chapters) = getParagraphsAndChapters paragraphsWithChapters
    bookName                         <- inlinesToString $ docTitle meta

    return $ Book linearParagraphs (M.fromList (zip [0..] chapters)) bookName
  where
    recursiveGetChapters :: [HeaderName] -> [Block] -> [([HeaderName], Paragraph)] -> PandocPure [([HeaderName], Paragraph)]
    recursiveGetChapters _ [] res                         = return res
    recursiveGetChapters names ((Div _ blocks') : xs) res = do
        newRes <- recursiveGetChapters names blocks' res
        recursiveGetChapters names xs newRes
    recursiveGetChapters names (h@Header{} : xs) res      = do
        headerName <- getNameOfHeader h
        recursiveGetChapters (modifyNames headerName names) xs res
    recursiveGetChapters names (p@Para{} : xs) res        = do
        newParagraph <- blockToParagraph p
        recursiveGetChapters names xs (res ++ [(names, newParagraph)])
    recursiveGetChapters names (_ : xs) res               = recursiveGetChapters names xs res

    modifyNames :: HeaderName -> [HeaderName] -> [HeaderName]
    modifyNames p@(n, _) = (++ [p]) . takeWhile ((/= n) . fst)

    inlinesToString :: [Inline] -> PandocPure String
    inlinesToString = blockToParagraph . Plain

    blockToParagraph :: Block -> PandocPure Paragraph
    blockToParagraph = fmap (unwords . splitOn "\n" . unpack) . writePlain def . Pandoc meta . pure

    getNameOfHeader :: Block -> PandocPure HeaderName
    getNameOfHeader (Header n _ inlines) = ((,) n) <$> inlinesToString inlines
    getNameOfHeader _                    = error "getNameOfHeader not on header."

    getParagraphsAndChapters :: [([String], Paragraph)] -> ([Paragraph], [([String], ParaNumber)])
    getParagraphsAndChapters parsChapts = res
      where
        pars               = fmap snd parsChapts
        chaptersWithParIds = zipWith (\(a, _) c -> (a, c)) parsChapts [0..]
        nubbedChapters'    = nubBy (\a b -> fst a == fst b) chaptersWithParIds
        nubbedChapters     = if null nubbedChapters' then [(["Chapter 0"], 0)] else nubbedChapters'

        res = insertPar nubbedChapters ([], pars, 0)

        insertPar :: [([String], ParaNumber)] -> ([([String], ParaNumber)], [Paragraph], Int) -> ([Paragraph], [([String], ParaNumber)])
        insertPar [] (chapts, pars', _)                        = (pars', chapts)
        insertPar ((names, ind) : xs) (chapts, pars', counter) | null names  = insertPar xs (newChapts, pars', counter)
                                                               | otherwise   = insertPar xs (newChapts, newPars, counter + 1)
          where
            curInd    = ind + counter
            newPars   = take curInd pars' ++ (last names : drop curInd pars')
            newChapts = chapts ++ [(names, curInd)]
