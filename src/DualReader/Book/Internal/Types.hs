module DualReader.Book.Internal.Types
  ( Book (..)
  , Chapter
  , Paragraph
  , ParaNumber
  , Content
  , SectionName
  , TableOfContent
  , ChapterInfo
  ) where

import           Data.Map.Strict (Map)


-- | Number of page in book.
--
type ParaNumber = Int

type Chapter = Int
type SectionName = String


type Paragraph = String
type Content   = [Paragraph]

type ChapterInfo = ([SectionName], ParaNumber)
type TableOfContent = Map Chapter ChapterInfo

-- | Our representation of book in form of paragraphers and list of contents.
--
data Book = Book { content        :: Content
                 , tableOfContent :: TableOfContent
                 , name           :: String
                 }
  deriving (Eq, Show)
