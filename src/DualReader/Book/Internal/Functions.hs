{-# LANGUAGE RecordWildCards #-}
module DualReader.Book.Internal.Functions
    ( getParagraphByChapter
    ) where

import qualified Data.Map.Strict                  as M (lookupLE)
import           DualReader.Book.Internal.Types

getChapterByParagraph :: Book -> ParaNumber -> Maybe Chapter
getChapterByParagraph Book{..} paragraph = snd . snd <$> M.lookupLE paragraph tableOfContent
