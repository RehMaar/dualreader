module DualReader.Book.State
    ( BookState (..)
    , BookContext (..)
    , getBookContext
    , moveToNextLine
    , moveToPrevLine
    , swapBooks
    , getContent
    , getName
    , getStatus 
    ) where

import           Data.List (intercalate)
import           Text.Printf

import           DualReader.Book
import           DualReader.Book.Sync

data BookState
    = BookState
    { book        :: Book
    , para        :: ParaNumber
    , maxParas    :: Int
    , strNum      :: Int
    }
    deriving Show

data BookContext
    = BookContext
    { mainBook       :: BookState
    , secondaryBook  :: BookState
    , marks          :: BookSyncMarks
    , isMainFront    :: Bool
    }
    deriving Show

getStatus :: BookContext -> String
getStatus context = printf "%03i/%03i" i n
  where
    fb = getFrontBook context
    n = maxParas fb
    i = para fb + 1

getBookContext :: Book -> Book -> BookSyncMarks -> BookContext
getBookContext bk1 bk2 m = BookContext
  { mainBook      = bState bk1
  , secondaryBook = bState bk2
  , marks         = m
  , isMainFront   = True
  }
 where
  bState bk = BookState {book = bk, para = 0, maxParas = length (content bk), strNum = 0}

--getParaCurrent :: BookContext -> Maybe Paragraph
--getParaCurrent = uncurry atMay . ((content . book) &&& para) . mainBook

getFrontBook :: BookContext -> BookState
getFrontBook bCtx | isMainFront bCtx = mainBook bCtx
                  | otherwise        = secondaryBook bCtx

--getBackBook :: BookContext -> BookState
--getBackBook bCtx | isMainFront bCtx = secondaryBook bCtx
--                 | otherwise        = mainBook bCtx

setFrontBook :: BookContext -> BookState -> BookContext
setFrontBook bCtx bState | isMainFront bCtx = bCtx { mainBook = bState }
                         | otherwise        = bCtx { secondaryBook = bState }

paragraphToLines :: Int -> String -> [String]
paragraphToLines maxWidth paragraph = reverse $ go 0 (words paragraph) [""]
  where
    go :: Int -> [String] -> [String] -> [String]
    go width words'@(w:ws) lines'@(line:rest) 
        | width + n + 1 <= maxWidth = go (width + n + 1) ws ((line ++ w ++ " "):rest)
        | otherwise                 = go 0 words' ("":lines')
      where
        n = length w
    go _ _ acc = acc

changeLine :: (Int -> Int) -> Int -> Int -> BookContext -> BookContext
changeLine f width _ bCtx = setFrontBook bCtx (frontBook { para = para', strNum = strNum' })
  where
    frontBook = getFrontBook bCtx
    pn = para frontBook
    p = paragraphToLines width (content (book frontBook) !! pn)
    n = length p + 1
    n' = f (strNum frontBook)
    (para', strNum') 
        | n' < 0  = if pn == 0 
                        then (0, 0)
                        else (pn - 1, length (paragraphToLines width (content (book frontBook) !! (pn - 1))) - 1)
        | n' >= n = if pn + 1 == maxParas frontBook
                        then (pn, strNum frontBook)
                        else (pn + 1, 0)
        | otherwise = (pn, n')

moveToPrevLine :: Int -> Int -> BookContext -> BookContext
moveToPrevLine width height bCtx = changeLine pred width height bCtx

moveToNextLine :: Int -> Int -> BookContext ->  BookContext
moveToNextLine width height bCtx = changeLine succ width height bCtx

getContent :: BookContext -> Int -> Int -> ([String], BookContext)
getContent bCtx width height = 
    (take height $ drop (strNum (getFrontBook bCtx)) 
     $ intercalate [""] $ drop (para (getFrontBook bCtx))
     $ map (paragraphToLines width) (content (book (getFrontBook bCtx)))
    , bCtx)

swapBooks :: BookContext -> BookContext
swapBooks bCtx =
    let front  = getFrontBook bCtx
        fbPara = para front
    in
        if (isMainFront bCtx)
        then
          let mark = findSyncMarkClever (marks bCtx) fbPara
          in case mark of
              Just (ch, par) -> changeContext ch par
              Nothing   -> bCtx { isMainFront = False }
        else
            bCtx { isMainFront = True }
       where
           changeContext _ parag =
            let oldSecondaryBook = secondaryBook bCtx
                newSecondaryBook = oldSecondaryBook { para = parag }
            in
              bCtx { secondaryBook = newSecondaryBook, isMainFront = False }


getName :: BookContext -> String
getName = name . book . getFrontBook
