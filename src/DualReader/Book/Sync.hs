module DualReader.Book.Sync
    ( BookSyncMarks
    , UserSyncMark
    , SyncMark
    , syncWithUserMarks
    , addNewMark
    , findSyncMark
    , findSyncMarkClever
    ) where

import           Data.List       (find, maximumBy)
import qualified Data.Map.Strict as M (empty, splitLookup)
import           Data.Maybe      (fromJust, isNothing)
import           DualReader.Book (Book (..), Chapter, ChapterInfo, ParaNumber,
                                  TableOfContent)

-- | ParaNumber is a paragraph number of the main and secondary second
-- | book which matches the same paragraph.
-- | The marks of the main book is the first member of pair and
-- | the marks of the secondary one is the second member.
type UserSyncMark = (Chapter, Chapter)

type SyncMark = (Chapter, ParaNumber)

type BookSyncMarks = [(SyncMark, SyncMark)]

syncWithUserMarks :: Book -> Book -> UserSyncMark -> BookSyncMarks
syncWithUserMarks book1 book2 (chapter1, chapter2) = res
 where
   tableOfContent1 = tableOfContent book1
   tableOfContent2 = tableOfContent book2

   res = sync' tableOfContent1 tableOfContent2 chapter1 chapter2

   splitMap :: Chapter -> TableOfContent -> Maybe (ChapterInfo, TableOfContent)
   splitMap mark toc = case M.splitLookup mark toc of
     (_, Just val, tocRest) -> Just (val, tocRest)
     _                      -> Nothing

   sync' :: TableOfContent -> TableOfContent -> Chapter -> Chapter -> BookSyncMarks
   sync' toc1 toc2 chpt1 chpt2 | toc1 == M.empty = []
                               | toc2 == M.empty = []
                               | otherwise = case (s1, s2) of
                                              (Just _, Just _) -> res'
                                              (_, _)           -> []
     where
       (s1, s2)                                   = (splitMap chpt1 toc1, splitMap chpt2 toc2)
       (Just (v1, tocRest1), Just (v2, tocRest2)) = (s1, s2)

       res' = ((chpt1, snd v1), (chpt2, snd v2)) : sync' tocRest1 tocRest2 (succ chpt1) (succ chpt2)

addNewMark :: BookSyncMarks -> UserSyncMark -> BookSyncMarks
addNewMark = undefined

findSyncMark :: BookSyncMarks -> ParaNumber -> Maybe SyncMark
findSyncMark syncMarks num | null candidates = Nothing
                           | otherwise       = res
    where
      firstSyncMarks = fmap fst syncMarks
      candidates     = filter ((<= num) . snd) firstSyncMarks

      neededChapter  = fst $ maximumBy (\a b -> snd a `compare` snd b) candidates

      res = snd <$> find ((== neededChapter) . fst . fst) syncMarks

findSyncMarkClever :: BookSyncMarks -> ParaNumber -> Maybe SyncMark
findSyncMarkClever syncMarks num | null candidates || isNothing syncMarkSndM = Nothing
                                 | otherwise                                 = res
    where
      firstSyncMarks           = fmap fst syncMarks
      candidates               = filter ((<= num) . snd) firstSyncMarks
      (_, paraFst)             = maximumBy (\a b -> snd a `compare` snd b) candidates

      syncMarkSndM          = findSyncMark syncMarks num
      (chapterSnd, paraSnd) = fromJust syncMarkSndM

      res = Just $ (chapterSnd, paraSnd + num - paraFst)
